import matplotlib.pyplot as plt 
import numpy as np
import csv

temp= dict()
tempset=set()
with open('/home/deepak/Downloads/primaryschool.csv','r') as csvfile:
    schooldata = csv.reader(csvfile)
    next(schooldata)
    print(schooldata) 
    for row in schooldata:
        # print(row)
        tempset.add(row[3])
        if row[8] in temp :
            if row[3] in temp[row[8]]:
                temp[row[8]][row[3]]+=1
            else:
                temp[row[8]][row[3]]=1
        else: 
            temp[row[8]]={}
            temp[row[8]][row[3]]=1

print(temp)  
dist=sorted(list(temp.keys()))
catlist=sorted(list(tempset))

tempdata=[]
for i  in range(len(catlist)):
    temp1=list()
    tempdata.append(temp1)
    

for key in dist:
    for i in range(len(catlist)):
        tempdata[i].append(temp[key].get(catlist[i],0))



plt.xticks(rotation=90)
plt.bar(dist, np.array(tempdata[0]), color='r')
y=np.array([0]*len(tempdata[0]))
clr=['y','b','g','#a5bccc','#6b30b8','#30b8ad','#b84730']
for i in range(len(tempdata)-1):
    y+= np.array(tempdata[i])
    temp3=np.array(tempdata[i+1])
    plt.bar(dist,temp3,bottom=y,color=clr[i])

plt.xlabel("Districts")
plt.ylabel("Languages ")
plt.legend(["other", "english", "hindi", "kannada","marathi","tamil","telugu","urdu"])
plt.title("No of Languages speak in each district")
plt.show()

