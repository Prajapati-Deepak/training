
import matplotlib.pyplot as plt 
import csv 

temp= {}
  
with open('/home/deepak/Downloads/primaryschool.csv','r') as csvfile:
    schooldata = csv.reader(csvfile)
    next(schooldata)
    print(schooldata) 
    for row in schooldata:
        # print(row)
        if row[8] in temp :
            temp[row[8]]+=1
        else: 
            temp[row[8]]=1    
  
x=list(temp.keys())
y=list(temp.values())

print(x)
print(y)
print(len(x))
print(len(y))
plt.xticks(rotation = 90)
plt.bar(x, y, color = 'r')
plt.xlabel('District')
plt.ylabel('Number of School')
plt.title('Number of School Per District')
plt.legend()
plt.show()
