import matplotlib.pyplot as plt 
import numpy as np
import csv

temp= dict()
tempset=set()
with open('/home/deepak/Downloads/primaryschool.csv','r') as csvfile:
    schooldata = csv.reader(csvfile)
    next(schooldata)
    print(schooldata) 
    for row in schooldata:
        # print(row)
        tempset.add(row[4])
        if row[8] in temp :
            if row[4] in temp[row[8]]:
                temp[row[8]][row[4]]+=1
            else:
                temp[row[8]][row[4]]=1
        else: 
            temp[row[8]]={}
            temp[row[8]][row[4]]=1

    
print(temp)
dist=sorted(list(temp.keys()))
catlist=sorted(list(tempset))
print(dist)
print(catlist)
tempdata=[]
for i  in range(len(catlist)):
    temp1=list()
    tempdata.append(temp1)
    

for key in dist:
    for i in range(len(catlist)):
        tempdata[i].append(temp[key].get(catlist[i],0))


y1 = np.array(tempdata[0])
y2 = np.array(tempdata[1])
y3 = np.array(tempdata[2])
y4 = np.array(tempdata[3])
plt.xticks(rotation=90)
plt.bar(dist, y1, color='r')
plt.bar(dist, y2, bottom=y1, color='b')
plt.bar(dist, y3, bottom=y1+y2, color='y')
plt.bar(dist, y4, bottom=y1+y2+y3, color='g')

plt.xlabel("Districts")
plt.ylabel("No of School")
plt.legend(["Lower Primary", "Model Primary", "Secondary", "Upper Primary"])
plt.title("School Category Present in Each District")
plt.show()
