import matplotlib.pyplot as plt 
import numpy as np
import csv

temp= dict()
tempset=set()

with open('/home/deepak/Downloads/matches.csv','r') as csvfile:
    matchdata = csv.reader(csvfile)
    next(matchdata)
    print(matchdata) 
    for row in matchdata:
        # print(row)
        tempset.add(row[4])
        tempset.add(row[5])
        if row[1] in temp:
            if row[4] in temp[row[1]]:
                temp[row[1]][row[4]]+=1
            else:
                temp[row[1]][row[4]]=1

            if row[5] in temp[row[1]]:
                temp[row[1]][row[5]]+=1
            else:
                temp[row[1]][row[5]]=1
               
        else: 
            temp[row[1]]=dict()
            temp[row[1]][row[4]]=1
            temp[row[1]][row[5]]=1

print(temp)  
dist=sorted(list(temp.keys()))
catlist=sorted(list(tempset)) 
print(dist)
print(catlist)
print(len(catlist))


tempdata=[]
for i  in range(len(catlist)):
    temp1=list()
    tempdata.append(temp1)


for key in dist:
    for i in range(len(catlist)):
        tempdata[i].append(temp[key].get(catlist[i],0))

print(tempdata)

# for


plt.xticks(rotation=90)
plt.bar(dist, np.array(tempdata[0]), color='r')
y=np.array([0]*len(tempdata[0]))
clr=['y','b','g','#a5bccc','#6b30b8','#30b8ad','#b84730','#f54263','#6f42f5','#f5ec42','#eb7134','#68eb34','#34ebbd','#eb34df']

for i in range(len(tempdata)-1):
    y+= np.array(tempdata[i])
    temp3=np.array(tempdata[i+1])
    plt.bar(dist,temp3,bottom=y,color=clr[i])

plt.xlabel("Year")
plt.ylabel("No of Matches played by teams")
plt.legend(catlist)
plt.show()