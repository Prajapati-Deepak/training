import matplotlib.pyplot as plt 
import csv 

temp= {}
  
with open('/home/deepak/Downloads/matches.csv','r') as csvfile:
    matches = csv.reader(csvfile)
    next(matches)
    print(matches) 
    for row in matches:
        if row[1] in temp:
            temp[row[1]]+=1
        else: 
            temp[row[1]]=1    
  
x=list(temp.keys())
y=list(temp.values())

print(x)
print(y)
print(len(x))
print(len(y))
plt.xticks(rotation = 90)
plt.bar(x, y, color = 'r')
plt.xlabel('Year')
plt.ylabel('Number of Matches')
plt.title('Number of Matches Per Year')
plt.legend()
plt.show()  
