import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainboxComponent } from './mainbox/mainbox.component';
import { NtkeeperComponent } from './ntkeeper/ntkeeper.component';

@NgModule({
  declarations: [
    AppComponent,
    MainboxComponent,
    NtkeeperComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
