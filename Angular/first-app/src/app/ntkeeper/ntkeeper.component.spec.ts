import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NtkeeperComponent } from './ntkeeper.component';

describe('NtkeeperComponent', () => {
  let component: NtkeeperComponent;
  let fixture: ComponentFixture<NtkeeperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NtkeeperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NtkeeperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
