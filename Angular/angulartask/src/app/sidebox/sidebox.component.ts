import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Note } from '../Interface/app.interface';
@Component({
  selector: 'app-sidebox',
  templateUrl: './sidebox.component.html',
  styleUrls: ['./sidebox.component.scss']
})
export class SideboxComponent {
   @Input() data:Note[]=[];
   @Input() slcted:string='';
   @Output() delelem=new EventEmitter<number>();
   @Output() emitupd= new EventEmitter<number>();
   
   isActive=true;
   searchinput:string='';

   deleteHandler(index:number){
     this.delelem.emit(index);
     console.log("from sidebox");
   }
   updateHandler(index:number){
    this.emitupd.emit(index);
   }
}
