import { Component,Input } from '@angular/core';

@Component({
  selector: 'app-onhold',
  templateUrl: './onhold.component.html',
  styleUrls: ['./onhold.component.scss']
})
export class OnholdComponent {
  @Input() data:string =' ';
  
}
