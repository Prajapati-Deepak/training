
export interface Note{
    message:string,
    color:string,
    date?:Date,
}

export interface UpdateNote{
    message:string,
    color:string,
    date?:Date ,
    index:number,
}


export interface UserData{
    message:string,
    color:string;
}



