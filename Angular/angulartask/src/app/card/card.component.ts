import { Component,EventEmitter,Input, Output } from '@angular/core';
import { Note } from '../Interface/app.interface';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
   @Input() userdata:Note={}as Note;
   @Input() index:number=0;
   @Output() deletedata= new EventEmitter<number>(); 
   @Output() emitupdate =new EventEmitter<number>();
   @Input() crdslcted:string='';

   deleteNote(index:number){
     console.log(index);
     this.deletedata.emit(index);
   }
   updateNote(index:number){
    // console.log("update from card");
      this.emitupdate.emit(index);
      // this.crdslcted='#dfcfb9';
   }

   
}
