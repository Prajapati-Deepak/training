import { Component, EventEmitter, Output } from '@angular/core';
import { Note, UpdateNote} from '../Interface/app.interface';
@Component({
  selector: 'app-mainbox',
  templateUrl: './mainbox.component.html',
  styleUrls: ['./mainbox.component.scss']
})
export class MainboxComponent {

  isActive=true;
  temp:Note[] =[];
  update:UpdateNote={} as UpdateNote;
  slcted:string ='';
  // messagemain:string='';
  // addnotemain:string='block';
  // updatentmain:string='none'; 

  NoteHandler(data:Note){
     console.log(data);
     this.temp.push(data);
  }
  deleteHandler(index:number){
   this.temp.splice(index,1);
  //  if(this.update?.index===index){
  //   console.log("deepak",this.update.index===index);
  //     this.messagemain='';
  //     this.addnotemain='blocfrgwk';
  //     this.updatentmain='nondffe';
  //  }
  }
  updateNote(ind:number){
    console.log("update from mainbox");
    let temp1:UpdateNote={
      ...this.temp[ind],
      index:ind  ,
    }
    this.update=temp1;
  }
  NoteUpdateHandler(data:UpdateNote){
     this.temp[data.index].message=data.message;
     this.temp[data.index].color=data.color;
     this.temp[data.index].date=data.date;
  }
}
