import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
const materialComponents=[
   MatButtonModule,
   MatCardModule,
   MatIconModule,
   MatFormFieldModule,
   MatInputModule,
   MatIconModule,
   MatSelectModule,
];

@NgModule({
  imports: [
    materialComponents
  ],
  exports:[
   materialComponents
  ]

})
export class MaterialModule { }
