import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainboxComponent } from './mainbox/mainbox.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import {MatDialogModule} from '@angular/material/dialog';
import { MaterialModule } from './material/material.module';
import { SideboxComponent } from './sidebox/sidebox.component';
import { TextboxComponent } from './textbox/textbox.component';
import { CardComponent } from './card/card.component';
import { DragdropComponent } from './dragdrop/dragdrop.component';
import { TodoComponent } from './todo/todo.component';
import { InprogressComponent } from './inprogress/inprogress.component';
import { OnholdComponent } from './onhold/onhold.component';
import { CompleteComponent } from './complete/complete.component';
@NgModule({
  declarations: [
    AppComponent,
    MainboxComponent,
    SideboxComponent,
    TextboxComponent,
    CardComponent,
    DragdropComponent,
    TodoComponent,
    InprogressComponent,
    OnholdComponent,
    CompleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
