
import { outputAst } from '@angular/compiler';
import { Component, EventEmitter ,Input,OnChanges,Output} from '@angular/core';
import { Note ,UpdateNote} from '../Interface/app.interface';
@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss']
})
export class TextboxComponent implements OnChanges {
  
   @Input() updVar:UpdateNote={ } as UpdateNote;
  //  @Input() message:string='';
  //  @Input() addnote:string='block';
  //  @Input() updatent:string='none';
   @Output() emitNote=new EventEmitter<Note>();
   @Output() emitUpdate=new EventEmitter<UpdateNote>();

    message = '';
    noteclr = '#00FF00';
    addnote='block';
    updatent='none';

    ngOnChanges(){
      if(this.updVar.date){
        console.log(this.updVar);
        this.message=this.updVar.message;
        this.noteclr=this.updVar.color;
        this.addnote='none';
        this.updatent='block';
      }
    }
     addNote(){
     console.log(this.message, this.noteclr);
     let dt= new Date ();
     let temp:Note={}as Note;
     temp ={
        message:this.message,
        color:this.noteclr,
        date:dt,
     }
     console.log(temp,"add note");
     this.emitNote.emit(temp);
     this.message='';
    }
    updateNote(){
      console.log(this.message, this.noteclr);
      let dt= new Date ();
      let temp:UpdateNote={}as UpdateNote;
      temp ={
         message:this.message,
         color:this.noteclr,
         date:dt,
         index:this.updVar.index,
      }
      this.emitUpdate.emit(temp);
      this.message='';
      this.addnote='block';
      this.updatent='none';
    }
}
