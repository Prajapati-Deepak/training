import requests as rq
import json

URL = "http://localhost:8000/studentapi/"


def get_data(id=None):
    data = {}
    if id is not None:
        data = {'id': id}
        
    json_data = json.dumps(data)
    req = rq.get(url=URL, data=json_data)
    data = req.json()
    print(data)

# get_data(6)


def post_data():
    data = {
        'name': 'Ravi',
        'roll': 104,
        'city': 'Dhanbad'
    }
    json_data = json.dumps(data)
    req = rq.post(url=URL, data=json_data)
    data = req.json()
    print(data)

# post_data()


def update_data():
    data = {
        'id': 5,
        'name': 'Rohitfkjd',
        'city': 'Ranchidsfsa',
    }
    json_data = json.dumps(data)
    req = rq.put(url=URL, data=json_data)
    data = req.json()
    print(data)

# update_data()


def delete_data():
    data = {
        'id': 12
    }
    json_data = json.dumps(data)
    r = rq.delete(url=URL, data=json_data)
    data = r.json()
    print(data)

# delete_data()
