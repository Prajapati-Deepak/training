from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django. contrib.auth import authenticate , login ,logout
from django.contrib.auth.hashers import make_password
from django.contrib import messages
def sign_up(req, *args, **kwargs):
    
    username=req.POST['username']
    firstname=req.POST['firstname']
    lastname=req.POST['lastname']
    email=req.POST['email']
    pass1=make_password(req.POST['password'])

    myuser=User.objects.create_user(username, email,pass1)
    myuser.first_name = firstname
    myuser.last_name  = lastname
    myuser.password   = pass1
    myuser.save()
    messages.success(req, "your account has been succesfully created ")
    return redirect('http://127.0.0.1:8000/authentication/signin/')
    

def sign_in(req, *args, **kwargs):
    
    username=req.POST['username']
    pass1=req.POST['password']
    print(username,pass1)
    user=authenticate(username=username,password=pass1)
    print(user)
    if user is not None:
        login(req,user)
        fname=user.first_name
        lname=user.get_email_field_name()
        username=user.get_username()
        return render(req, 'authentication/signout.html',{'fname':fname,'lname':lname, 'username':username})

    else:

        messages.error(req, "Bad credentials")
        return redirect('http://127.0.0.1:8000/authentication/')


def sign_out(req, *args, **kwargs):
    
    logout(req)
    messages.success(req, "logged out successfully")
    return redirect('http://127.0.0.1:8000/authentication/')