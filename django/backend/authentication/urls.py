from django.urls import path, include
from .views import AuthenticationAPI


urlpatterns = [
    path('signup/',  AuthenticationAPI.as_view()),
    path('signout/', AuthenticationAPI.as_view()),
    path('signin/',  AuthenticationAPI.as_view()),
    path('', AuthenticationAPI.as_view())
]