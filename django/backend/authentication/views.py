from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View
from .service import sign_out, sign_in, sign_up


class AuthenticationAPI(View):


    def post(self, request, *args, **kwargs):
        print(request)
          
        if(request.path=='/authentication/signup/'):
            return sign_up(request)
        if(request.path=='/authentication/signin/'):
            return sign_in(request)
        else:
            return HttpResponse("Something went wrong")

    def get(self, request, *args, **kwargs):

        if(request.path=='/authentication/signup/'):
            return render(request,'authentication/signup.html')
        elif(request.path=='/authentication/signin/'):
            return render(request,'authentication/signin.html')
        elif(request.path=='/authentication/signout/'):
            return sign_out(request)
        else:
            return render(request,'authentication/mainpage.html')
    
    
