from django.contrib import admin
from .models import Requesttime

@admin.register(Requesttime)
class RequesttimeAdmin(admin.ModelAdmin):
    list_display = ['id','timetaken']