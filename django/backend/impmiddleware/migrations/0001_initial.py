# Generated by Django 3.2.16 on 2023-01-31 03:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Requesttime',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timetaken', models.DecimalField(decimal_places=7, max_digits=10, verbose_name='timetaken(s)')),
            ],
        ),
    ]
