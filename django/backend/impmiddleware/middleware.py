from .models import Requesttime
import time
class Mymiddleware:
    
    def __init__(self, get_response):
        self.get_response=get_response
    
    def __call__(self, request):

       startime=time.time()
       response=self.get_response(request)
       duration=time.time()-startime
       timeobj= Requesttime(timetaken=duration)
       timeobj.save()
       return response



