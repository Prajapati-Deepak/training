from django.db import models

class Requesttime(models.Model):
    timetaken=models.DecimalField( max_digits=10, decimal_places=7,verbose_name="timetaken(s)")

    def __str__(self):
        return self.timetaken