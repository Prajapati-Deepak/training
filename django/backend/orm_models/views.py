from django.shortcuts import render
from django.views import View
from .service import fetchdata
class ModelpracticeAPI(View):

     def get(self, request, *args, **kwargs):
        return fetchdata()
 
  