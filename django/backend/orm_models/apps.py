from django.apps import AppConfig


class OrmModelsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'orm_models'
