from django.db import models


class ModelPractice(models.Model):
    username = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=75)

    def __str__(self):
        return self.first_name


class Category(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class Hero(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    benevolence_factor = models.PositiveSmallIntegerField(
        help_text="How benevolent this hero is?",
        default=50
    )
    def __str__(self):
        return self.name