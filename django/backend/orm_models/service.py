
from django.http import HttpResponse
from .models import ModelPractice, Hero, Category
from django.db.models import Subquery, OuterRef
from django.core import serializers
from django.db.models import Q


def fetchdata():
    queryset = ModelPractice.objects.all()
    # qsetjson= serializers.serialize("json", queryset)

    # print(qsetjson)
    # return HttpResponse(temp)
    queryset1 = ModelPractice.objects.filter(id__gt=5)
    # str(queryset1.query)
    # print(queryset1)

    # how to do or queries in django orm

    queryset3 = ModelPractice.objects.filter(
        first_name__startswith='R'
    ) | ModelPractice.objects.filter(last_name__startswith='D')

    # second method

    qs = ModelPractice.objects.filter(
        Q(first_name__startswith='R') | Q(last_name__startswith='D'))

    # How to do AND  queries in Django orm

    queryset_1 = ModelPractice.objects.filter(
        first_name__startswith='R',
        last_name__startswith='D'
    )
    # print(queryset_1)

    queryset_2 = ModelPractice.objects.filter(
        first_name__startswith='R'
    ) & ModelPractice.objects.filter(
        last_name__startswith='D'
    )
    # print(queryset_2)

    queryset_3 = ModelPractice.objects.filter(
        Q(first_name__startswith='R') &
        Q(last_name__startswith='D')
    )
    # print(queryset_3)

    #  How to do a NOT query in Django queryset?

    querysetnot = ModelPractice.objects.filter(~Q(id__lt=5))
    # print(querysetnot)

    # How to do union of two querysets from same or different models?

    q1 = ModelPractice.objects.filter(id__gte=5)
    # print(q1)
    q2 = ModelPractice.objects.filter(id__lte=9)
    # print(q2)

    # print(q1.union(q2))
    # print(q2.union(q1))

    # How to select some fields only in a queryset?

    queryset5 = ModelPractice.objects.filter(
        first_name__startswith='R'
    ).values('first_name', 'last_name')

    print(queryset5)


#    How to do a subquery expression in Django?

    hero_qs = Hero.objects.filter(
        category=OuterRef("pk")
    ).order_by("-benevolence_factor")

    result= Category.objects.all().annotate(
        most_benevolent_hero=Subquery(
            hero_qs.values('name')[:1]
        )
    )
    print(result.values('id','name'))





    #


    return HttpResponse("data printed on terminal ")
