from django.contrib import admin
from .models import ModelPractice, Category, Hero


@admin.register(ModelPractice)
class ModelPracticeAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'email']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Hero)
class HeroAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'category', 'benevolence_factor']
