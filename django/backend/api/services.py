from django.http import HttpResponse, JsonResponse
from rest_framework.renderers import JSONRenderer
from .serializers import StudentSerializer
from .models import Student
import json


def fetchdata(json_data):
    pythondata = json.loads(json_data)
    print(pythondata)
    id = pythondata.get('id', None)

    if id is not None:
        student = Student.objects.get(id=id)
        serializer = StudentSerializer(student)
        json_data = JSONRenderer().render(serializer.data)
        return HttpResponse(json_data, content_type='application/json')

    student = Student.objects.all()
    serializer = StudentSerializer(student, many=True)
    json_data = JSONRenderer().render(serializer.data)
    return HttpResponse(json_data, content_type='application/json')


def createdata(json_data):
    pythondata = json.loads(json_data)
    print(pythondata)
    serializer = StudentSerializer(data=pythondata)

    if serializer.is_valid():
        serializer.save()
        res = {'msg': 'Data Created'}
        json_data = JSONRenderer().render(res)
        return HttpResponse(json_data, content_type="application/json")

    json_data = JSONRenderer().render(serializer.errors)
    return HttpResponse(json_data, content_type="application/json")


def updatedata(json_data):
    pythondata = json.loads(json_data)
    id = pythondata.get('id', None)
    student = Student.objects.get(id=id)
    serializer = StudentSerializer(student, data=pythondata, partial=True)

    if serializer.is_valid():
        serializer.save()
        res = {'msg': 'Data Updated!!'}
        json_data = JSONRenderer().render(res)
        return HttpResponse(json_data, content_type="application/json")

    json_data = JSONRenderer.render(serializer.errors)
    return HttpResponse(json_data, content_type='application/json')


def deletedata(json_data):
    pythondata = json.loads(json_data)
    id = pythondata.get('id', None)
    student = Student.objects.get(id=id)
    student.delete()
    res = {'msg': 'Data Deleted !!'}
    return JsonResponse(res, safe=False)
