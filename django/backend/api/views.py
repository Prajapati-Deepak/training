
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View

from .services import fetchdata, createdata, updatedata, deletedata

# @method_decorator(csrf_exempt, name='dispatch')


class StudentAPI(View):

    def get(self, request, *args, **kwargs):
        return fetchdata(request.body)

    def post(self, request, *args, **kwargs):
        return createdata(request.body)

    def put(self, request, *args, **kwargs):
        return updatedata(request.body)

    def delete(self, request, *args, **kwargs):
        return deletedata(request.body)
