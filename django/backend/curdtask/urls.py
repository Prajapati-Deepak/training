
from django.contrib import admin
from django.urls import path, include
from api import views as view1
from orm_models import views as view2
from homepage import views as homeview



urlpatterns = [
    path('admin/', admin.site.urls),
    path('studentapi/', view1.StudentAPI.as_view()),
    path('modelpractice/',view2.ModelpracticeAPI.as_view()),
    path('authentication/',include("authentication.urls")),
    path('', homeview.Homepage.as_view())
]
